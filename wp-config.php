<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'zetta');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=k`Z(#tvS{*5L9dyS+5.v(@/gRED}JVNRH:neIQ3GNkDg90YP 2~~.0KHWPh9tio');
define('SECURE_AUTH_KEY',  'TrNo<;~[,yfg2`fa8ZjuWxC-T[Rds+/b:-By5VY5go(Eef#Zh[ZMpB%Z_~,j`tVv');
define('LOGGED_IN_KEY',    'gAiR$1uhM+57tm~)i.05AVqNMJ.W{+,nEH!NwMF ]z]?1l[F  yzigZcoM/S,Ix7');
define('NONCE_KEY',        '$.Gu@HyAq!9~m7bJ#pV+t`k5XH_v@*/`ktJNITE@fe-x)gL[}.&uzb)(nDa&,I`p');
define('AUTH_SALT',        'c=sml3-_B1)bOKp7r+LI:p%Q2`f=6CF!}D*B(8t_Lfe1C-dDSTV};r4k`7{56vBE');
define('SECURE_AUTH_SALT', 'WN2[&WDm9-IXFZ*=BJFIWy$#;Z8ekH1(Wj<;is]@9Z:/ysPJpB;n+;q@PX:<R5cy');
define('LOGGED_IN_SALT',   'U&A61lJ?~pI:z;|ytqC^fc!TfHLR^G(@s^wv/#&#qm9`Sg*u~ML +Ihc|U[*TG@#');
define('NONCE_SALT',       '~.5 v7L9l=XEJ)8!c<F`ZiPz+{*nys`<-_RAv[U~H0RLiBuue]sUQ*6dE*GcBkI&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
